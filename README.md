<h2>DEPENDENCIES</h2>

<p> You should have <a href="https://ffmpeg.org/download.html">ffmpeg</a> installed</p>


<h2>INSTRUCTIONS</h2>
<ol>
<li>Add the script to the folder containing <strong>.mkv</strong> files <i>(it will automatically detect mkv files and ignore other formats)</i></li>
<li>Run the script....but before that make sure you assign excecuting permisions to the script</li>
    
    chmod u+x nibble.sh
    
<li>Run the script</li>    
    
    ./nibble.sh

<li>All your converted files will be in a folder called <strong>converted_files</strong></li>    
</ol>