#!/bin/bash
Blue='\033[0;34m'
Color_Off='\033[0m'
declare -i num=1
for i in *
do
	if [[ $i == *'.mkv' || $i == *'.MKV' ]]; 
	then
		if [ ! -d "converted_files" ]; 
		then
			mkdir converted_files
		fi
		mods=${i//mkv/mp4}
		echo -e "Working on ${Blue}$i${Color_Off}"
		ffmpeg -i "$i" -codec copy converted_files/"$mods" -loglevel quiet
		first='File '
		second=' has been converted'
		display="$first$num$second"
		echo $display
		let 'num++'
	fi	
done
